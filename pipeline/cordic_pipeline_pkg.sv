/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cordic_pipeline_pkg;
	timeunit      1ns;
	timeprecision 1ps;


	import cordic_pkg::*;


	typedef struct {
		data_int_at re;
		data_int_at im;
		data_int_at pha;
		quadrant_et quadrant;
		mode_et     mode;
		logic       ready;
		logic       valid;
	} stage_st;

	const stage_st STAGE_RST = '{
		/* re       = */ 0,
		/* im       = */ 0,
		/* pha      = */ 0,
		/* quadrant = */ Q_1,
		/* mode     = */ c_mode.first(),
		/* ready    = */ 0,
		/* valid    = */ 0
	};

	const mode_et c_mode = MODE_ROTATE;
	
	const int unsigned N_SCALE_SHIFT [1:8] = '{
		1, 3, 6, 8, 10, 11, 12, 14
	};

	typedef enum {
		SCALE_ADD,
		SCALE_SUB
	} scale_op_t;

	const scale_op_t SCALE_OPS [1:8] = '{
		SCALE_ADD, SCALE_ADD, SCALE_SUB, SCALE_SUB,
		SCALE_ADD, SCALE_ADD, SCALE_ADD, SCALE_ADD
	};

endpackage
