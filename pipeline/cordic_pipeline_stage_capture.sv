/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import cordic_pipeline_pkg::*;

module cordic_pipeline_stage_capture (
	cordic_if.sink         i,
	cordic_stage_if.source o
);
	timeunit      1ns;
	timeprecision 1ps;

	import cordic_pkg::*;

	stage_st r, w;

	always_comb
	begin
		w.ready <= 1;

		if (i.valid) begin
			w.mode <= i.mode;

			case (i.mode)
			MODE_ROTATE:
			begin : mode_rotate
				w.re <= {i.re_mag[$high(i.re_mag)], i.re_mag <<< i.N_EXPAND};
				w.im <= 0;
				w.pha <= {i.im_pha[$high(i.im_pha)], i.im_pha <<< i.N_EXPAND};
			end : mode_rotate
			MODE_VECTOR:
			begin : mode_vector
				w.re <= {i.re_mag[$high(i.re_mag)], i.re_mag <<< i.N_EXPAND};
				w.im <= {i.im_pha[$high(i.im_pha)], i.im_pha <<< i.N_EXPAND};
				w.pha <= 0;
			end : mode_vector
			endcase

			w.valid <= 1;
		end else begin
			w.valid <= 0;
		end

	end

	always_ff @(posedge i.clk, posedge i.rst)
	begin
		if (i.rst) begin
			r <= STAGE_RST;
		end else begin
			r <= w;
		end
	end

	assign o.stage = r;

endmodule
