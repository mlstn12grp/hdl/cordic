/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
module cordic_arch_pipelined(
	cordic_if.sink   i,
	cordic_if.source o
);
	timeunit      1ns;
	timeprecision 1ps;


	import cordic_pkg::*;
	import cordic_pipeline_pkg::*;
	
	cordic_stage_if w_capture   (i.rst, i.clk);
	cordic_stage_if w_normalize (i.rst, i.clk);
	cordic_stage_if w_rotate    (i.rst, i.clk);
	cordic_stage_if w_scale     (i.rst, i.clk);
	cordic_stage_if w_finalize  (i.rst, i.clk);


	assign i.ready = w_capture.stage.ready;


	cordic_pipeline_stage_capture s_capture (
		.i(i),
		.o(w_capture)
	);

	cordic_pipeline_stage_normalize s_normalize (
		.i(w_capture),
		.o(w_normalize)
	);


	genvar g_stage;

	generate
	begin : rotate
		stage_st w_stages[$size(ATAN_TABLE)];

		for (g_stage = 0; g_stage < $size(w_stages); ++g_stage)
		begin
			cordic_stage_if _i (i.rst, i.clk);
			cordic_stage_if _o (i.rst, i.clk);

			case (g_stage)
			$low(ATAN_TABLE): begin
				assign _i.stage = w_normalize.stage;
			end
			default: begin
				assign _i.stage = w_stages[g_stage - 1];
			end
			endcase

			assign w_stages[g_stage] = _o.stage;

			cordic_pipeline_stage_rotate #(
				.G_STAGE(g_stage)
			) s_rotate (
				.i(_i),
				.o(_o)
			);
		end

		assign w_rotate.stage = w_stages[$high(w_stages)];
	end : rotate
	endgenerate

	generate
	begin : scale
		typedef struct {
			stage_st main;
			stage_st aux;
		} stage_st;
		stage_st w_stages[$left(N_SCALE_SHIFT):$right(N_SCALE_SHIFT)];

		for (g_stage = $left(w_stages); g_stage <= $right(w_stages); ++g_stage)
		begin
			cordic_stage_if _i     (i.rst, i.clk);
			cordic_stage_if _i_aux (i.rst, i.clk);
			cordic_stage_if _o     (i.rst, i.clk);
			cordic_stage_if _o_aux (i.rst, i.clk);

			case (g_stage)
			$left(w_stages): begin
				always_comb
				begin
					_i.stage    <= w_rotate.stage;
					_i.stage.re <= 0;
					_i.stage.im <= 0;
				end
				assign _i_aux.stage = w_rotate.stage;
			end
			default: begin
				assign _i.stage     = w_stages[g_stage - 1].main;
				assign _i_aux.stage = w_stages[g_stage - 1].aux;
			end
			endcase

			assign w_stages[g_stage].main = _o.stage;
			assign w_stages[g_stage].aux  = _o_aux.stage;

			cordic_pipeline_stage_scale #(
				.G_STAGE(g_stage)
			) s_scale (
				.i    (_i),
				.i_aux(_i_aux),
				.o    (_o),
				.o_aux(_o_aux)
			);
		end

		always_comb
		begin
			int unsigned n_scale_sel = $high(w_stages);

			for (n_scale_sel = $low(w_stages); n_scale_sel < $high(w_stages); ++n_scale_sel)
			begin
				if (N_SCALE_SHIFT[n_scale_sel] > $size(ATAN_TABLE[0])) begin
					--n_scale_sel;
					break;
				end
			end

			w_scale.stage <= w_stages[n_scale_sel].main;
		end
	end : scale
	endgenerate

	cordic_pipeline_stage_finalize s_finalize (
		.i(w_scale),
		.o(o)
	);

endmodule
