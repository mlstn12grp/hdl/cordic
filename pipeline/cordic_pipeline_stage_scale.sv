/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import cordic_pipeline_pkg::*;

module cordic_pipeline_stage_scale #(
	parameter G_STAGE
)(
	cordic_stage_if.sink   i,
	cordic_stage_if.sink   i_aux,
	cordic_stage_if.source o,
	cordic_stage_if.source o_aux
);
	timeunit      1ns;
	timeprecision 1ps;

	import cordic_pkg::*;

	const int unsigned n_shift = N_SCALE_SHIFT[G_STAGE];

	stage_st r, r_aux, w;
	stage_st w_prev;

	always_comb
	begin
		w_prev = i.stage;
		w    <= w_prev;

		case (SCALE_OPS[G_STAGE])
		SCALE_ADD: begin
			w.re <= w_prev.re + (i_aux.stage.re >>> n_shift);
			w.im <= w_prev.im + (i_aux.stage.im >>> n_shift);
		end
		SCALE_SUB: begin
			w.re <= w_prev.re - (i_aux.stage.re >>> n_shift);
			w.im <= w_prev.im - (i_aux.stage.im >>> n_shift);
		end
		endcase

		case (w_prev.mode)
		MODE_VECTOR: begin
			/* Phase truncation in vector mode */
			case (w_prev.pha[$high(w_prev.pha):$high(w_prev.pha)-1])
			2'b11: begin
				w.pha <= 0;
			end
			2'b10: begin
				w.pha <= -1;
				w.pha[$high(w.pha)] <= 1'b0;
			end
			default: begin
			end
			endcase
		end
		endcase
	end

	always_ff @(posedge i.clk, posedge i.rst)
	begin
		if (i.rst) begin
			r     <= STAGE_RST;
			r_aux <= STAGE_RST;
		end else begin
			r     <= w;
			r_aux <= i_aux.stage;
		end
	end

	assign o.stage     = r;
	assign o_aux.stage = r_aux;

endmodule
