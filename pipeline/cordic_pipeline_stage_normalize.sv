/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import cordic_pipeline_pkg::*;

module cordic_pipeline_stage_normalize (
	cordic_stage_if.sink   i,
	cordic_stage_if.source o
);
	timeunit      1ns;
	timeprecision 1ps;

	import cordic_pkg::*;

	stage_st r, w;
	stage_st w_prev;

	always_comb
	begin
		w_prev = i.stage;
		w <= w_prev;

		case (w_prev.mode)
		MODE_ROTATE:
		begin : mode_rotate
			unique case (w_prev.pha[$high(w_prev.pha)-1:$high(w_prev.pha)-2])
			2'b00: begin
				w.quadrant <= Q_1;
			end
			2'b01: begin
				w.quadrant <= Q_2;
			end
			2'b10: begin
				w.quadrant <= Q_3;
			end
			2'b11: begin
				w.quadrant <= Q_4;
			end
			default: begin
			end
			endcase
			w.pha <= w_prev.pha <<< 2;
			w.pha[$high(w.pha)] <= 0;
		end : mode_rotate
		MODE_VECTOR:
		begin : mode_vector
			unique case ({w_prev.re[$high(w_prev.re)], w_prev.im[$high(w_prev.im)]})
			2'b00: begin
				w.quadrant <= Q_1;
			end
			2'b10: begin
				w.quadrant <= Q_2;
				w.re <=  w_prev.im;
				w.im <= -w_prev.re;
			end
			2'b11: begin
				w.quadrant <= Q_3;
				w.re <= -w_prev.re;
				w.im <= -w_prev.im;
			end
			2'b01: begin
				w.quadrant <= Q_4;
				w.re <= -w_prev.im;
				w.im <=  w_prev.re;
			end
			default: begin
			end
			endcase
		end : mode_vector
		endcase

		w.valid <= w_prev.valid;

	end

	always_ff @(posedge i.clk, posedge i.rst)
	begin
		if (i.rst) begin
			r <= STAGE_RST;
		end else begin
			r <= w;
		end
	end

	assign o.stage = r;

endmodule
