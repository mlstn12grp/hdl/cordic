/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import cordic_pipeline_pkg::*;

module cordic_pipeline_stage_finalize(
	cordic_stage_if.sink   i,
	cordic_if.source o
);
	timeunit      1ns;
	timeprecision 1ps;

	import cordic_pkg::*;

	stage_st r, w;
	stage_st w_prev;

	always_comb
	begin
		w_prev    = i.stage;
		w_prev.re = i.stage.re <<< o.N_EXPAND;
		w_prev.im = i.stage.im <<< o.N_EXPAND;
		w <= w_prev;

		case (w_prev.mode)
		MODE_ROTATE:
		begin : mode_rotate
			unique case (w_prev.quadrant)
			Q_1: begin
			end
			Q_2: begin
				w.re <= -w_prev.im;
				w.im <=  w_prev.re;
			end
			Q_3: begin
				w.re <= -w_prev.re;
				w.im <= -w_prev.im;
			end
			Q_4: begin
				w.re <=  w_prev.im;
				w.im <= -w_prev.re;
			end
			endcase
		end : mode_rotate
		MODE_VECTOR:
		begin : mode_vector
			unique case (w_prev.quadrant)
			Q_1: begin
				w.pha <= (w_prev.pha >>> 2);
				w.im  <= (w_prev.pha >>> 2);
			end
			Q_2: begin
				w.pha <= (w_prev.pha >>> 2) + (2 ** ($size(o.re_mag) - 2));
				w.im  <= (w_prev.pha >>> 2) + (2 ** ($size(o.re_mag) - 2));
			end
			Q_3: begin
				w.pha <= (w_prev.pha >>> 2) - (2 ** ($size(o.re_mag) - 1));
				w.im  <= (w_prev.pha >>> 2) - (2 ** ($size(o.re_mag) - 1));
			end
			Q_4: begin
				w.pha <= (w_prev.pha >>> 2) - (2 ** ($size(o.re_mag) - 2));
				w.im  <= (w_prev.pha >>> 2) - (2 ** ($size(o.re_mag) - 2));
			end
			endcase
			/* Magnitude truncation */
			unique case (w_prev.re[$high(w_prev.re):$high(w_prev.re)-1])
			2'b01: begin
				w.re <= -1;
				w.re[$high(w.re):$high(w.re)-1] <= 2'b00;
			end
			2'b10: begin
				w.re <= 0;
				w.re[$high(w.re):$high(w.re)-1] <= 2'b11;
			end
			default: begin
			end
			endcase
		end : mode_vector
		endcase

		w.valid <= w_prev.valid;

	end

	always_ff @(posedge i.clk, posedge i.rst)
	begin
		if (i.rst) begin
			r <= STAGE_RST;
		end else begin
			r <= w;
		end
	end

	assign o.mode   = r.mode;
	assign o.valid  = r.valid;
	assign o.re_mag = r.re;
	assign o.im_pha = r.im;

endmodule
