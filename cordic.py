# Copyright 2021 Alexander Preissner
# SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
#
# Licensed under the Solderpad Hardware License v 2.1 (the “License”);
# you may not use this file except in compliance with the License, or, at your
# option, the Apache License version 2.0.
# You may obtain a copy of the License at
#
# https://solderpad.org/licenses/SHL-2.1/
#
# Unless required by applicable law or agreed to in writing, any work
# distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import math


class CORDIC():

    def __init__(self, n: int, w: int):
        self.n = n
        self.w = w
        self.atans = None
        self.K = 1
        self.K_i = 1

        self.create_atan_table()
        self.calc_scaling_factor()

    def create_atan_table(self):
        atan = {}

        for i in range(self.n):
            atan[2 ** (-i)] = dict()
            atan[2 ** (-i)]['rad'] = math.atan(2 ** (-i))
            atan[2 ** (-i)]['deg'] = math.degrees(atan[2 ** (-i)]['rad'])
            atan[2 ** (-i)]['int'] = int(atan[2 ** (-i)]['rad'] / (math.pi / 2) * (2 ** self.n))

        self.atans = atan

    def verilog_atan_table(self, path):
        with open(path, "w") as f:
            # f.write("package cordic_table_pkg;\n")
            f.write("const bit [0:{:d}][{:d}:0] ATAN_TABLE".format(self.n-1, self.n-1) + " = '{\n")
            for key in self.atans:
                f.write(("\t{:d}'b{" + ":0{:d}b".format(self.n) + "}").format(self.n, self.atans[key]['int']))
                if key != 2 ** (-self.n+1):
                    f.write(",")
                f.write("\n")
            f.write("};\n")
            # f.write("endpackage")
            f.close()

    def calc_scaling_factor(self):
        for i in range(self.n):
            self.K *= math.cos(self.atans[2 ** (-i)]['rad'])

        self.K_i = int(self.K * (2 ** self.n))

    def rotate(self, deg):
        while deg > 180:
            deg -= 360
        while deg <= -180:
            deg += 360

        if 0 <= deg < 90:
            quadrant = 1
        elif 90 <= deg < 180:
            quadrant = 2
            deg -= 90
        elif -180 <= deg < -90:
            quadrant = 3
            deg += 180
        else:
            quadrant = 4
            deg += 90

        rad = math.radians(deg)
        start = {
            'x': 1.0,
            'y': 0.0,
            'z': rad
        }
        v = {0: start}

        for i in range(self.n):
            if v[i]['z'] <= 0:
                sigma = -1
            else:
                sigma = 1

            v[i + 1] = {
                'x': v[i]['x'] - sigma * (2 ** (-i)) * v[i]['y'],
                'y': v[i]['y'] + sigma * (2 ** (-i)) * v[i]['x'],
                'z': v[i]['z'] - sigma * self.atans[2 ** (-i)]['rad'],
            }

        result = {
            'x': v[self.n]['x'] * self.K,
            'y': v[self.n]['y'] * self.K,
        }

        if quadrant == 1:
            pass
        elif quadrant == 2:
            result = {
                'x': -result['y'],
                'y': result['x']
            }
        elif quadrant == 3:
            result = {
                'x': -result['x'],
                'y': -result['y']
            }
        else:
            result = {
                'x': result['y'],
                'y': -result['x']
            }

        return result

    def rotate_int(self, rad):
        while rad > (2 ** (self.w - 1)):
            rad -= (2 ** self.w)
        while rad <= -(2 ** (self.w - 1)):
            rad += (2 ** self.w)

        if 0 <= rad < (2 ** (self.w - 2)):
            quadrant = 1
        elif (2 ** (self.w - 2)) <= rad < (2 ** (self.w - 1)):
            quadrant = 2
            rad -= (2 ** (self.w - 2))
        elif -(2 ** (self.w - 1)) <= rad < -(2 ** (self.w - 2)):
            quadrant = 3
            rad += (2 ** (self.w - 1))
        else:
            quadrant = 4
            rad += (2 ** (self.w - 2))

        start = {
            'x': 1 << (self.n - 1),
            'y': 0,
            'z': rad << (self.n - self.w + 2)
            # 'z': deg
        }
        v = {0: start}

        for i in range(self.n):
            if v[i]['z'] <= 0:
                sigma = -1
            else:
                sigma = 1

            v[i + 1] = {
                'x': int(v[i]['x'] - sigma * (v[i]['y'] >> i)),
                'y': int(v[i]['y'] + sigma * (v[i]['x'] >> i)),
                'z': v[i]['z'] - sigma * self.atans[2 ** (-i)]['int'],
                # 'z': v[i - 1]['z'] - sigma * self.atans[2 ** (-i)]['deg'],
                'deg': self.atans[2 ** (-i)]['deg'],
                'd': self.atans[2 ** (-i)]['int']
            }
            v[i]['ang'] = v[i]['z'] / (2 ** 16) * 45.0

        # Below is an approximation of scaling factor K for n >= 8
        result = {
            'x': (
                         (v[self.n]['x'] >> 1)
                         + (v[self.n]['x'] >> 3)
                         - (v[self.n]['x'] >> 6)
                         - (v[self.n]['x'] >> 8)
                         + (v[self.n]['x'] >> 10)
                         + (v[self.n]['x'] >> 11)
                         + (v[self.n]['x'] >> 12)
                         + (v[self.n]['x'] >> 14)
                 ) >> (
                        self.n - self.w),
            'y': (
                         (v[self.n]['y'] >> 1)
                         + (v[self.n]['y'] >> 3)
                         - (v[self.n]['y'] >> 6)
                         - (v[self.n]['y'] >> 8)
                         + (v[self.n]['y'] >> 10)
                         + (v[self.n]['y'] >> 11)
                         + (v[self.n]['y'] >> 12)
                         + (v[self.n]['y'] >> 14)
                 ) >> (
                        self.n - self.w),
        }

        if quadrant == 1:
            pass
        elif quadrant == 2:
            result = {
                'x': -result['y'],
                'y': result['x']
            }
        elif quadrant == 3:
            result = {
                'x': -result['x'],
                'y': -result['y']
            }
        else:
            result = {
                'x': result['y'],
                'y': -result['x']
            }

        return result

    def get_angle(self, x, y):
        if x >= 0 and y >= 0:
            quadrant = 1
        elif x < 0 and y >= 0:
            quadrant = 2
            x = -x
        elif x < 0 and y < 0:
            quadrant = 3
            x = -x
            y = -y
        else:
            quadrant = 4
            y = -y

        start = {
            'x': x,
            'y': y,
            'z': 0.0
        }
        v = {0: start}

        for i in range(self.n):
            if v[i]['y'] < 0:
                sigma = -1
            else:
                sigma = 1

            v[i + 1] = {
                'x': v[i]['x'] + sigma * (2 ** (-i)) * v[i]['y'],
                'y': v[i]['y'] - sigma * (2 ** (-i)) * v[i]['x'],
                'z': v[i]['z'] + sigma * self.atans[2 ** (-i)]['rad'],
            }

        z = v[self.n]['z']
        z = math.degrees(z)

        if quadrant == 1:
            pass
        elif quadrant == 2:
            z += 90
        elif quadrant == 3:
            z -= 180
        else:
            z -= 90

        return z

    def get_angle_int(self, x, y):
        if x >= 0 and y >= 0:
            quadrant = 1
        elif x < 0 and y >= 0:
            quadrant = 2
            x = -x
        elif x < 0 and y < 0:
            quadrant = 3
            x = -x
            y = -y
        else:
            quadrant = 4
            y = -y

        start = {
            'x': x,
            'y': y,
            'z': 0
        }
        v = {0: start}

        for i in range(self.n):
            if v[i]['y'] < 0:
                sigma = -1
            else:
                sigma = 1

            v[i + 1] = {
                'x': v[i]['x'] + sigma * (v[i]['y'] >> i),
                'y': v[i]['y'] - sigma * (v[i]['x'] >> i),
                'z': v[i]['z'] + sigma * self.atans[2 ** (-i)]['int'],
            }

        z = v[self.n]['z'] >> 2

        if quadrant == 1:
            pass
        elif quadrant == 2:
            z += (2 ** (self.w - 2))
        elif quadrant == 3:
            z -= (2 ** (self.w - 1))
        else:
            z -= (2 ** (self.w - 2))

        return z
