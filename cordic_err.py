import numpy as np
import matplotlib.pyplot as plt
from prettytable import PrettyTable
import cordic

n_bits = 16
n_samples = 2**n_bits

c = cordic.CORDIC(n_bits, n_bits)

phase   = np.linspace(-180, (180 - 360/(2**n_bits)), n_samples)
phase_i = np.linspace(-(2 ** (n_bits - 1)), (2 ** (n_bits - 1)) - 1, n_samples, dtype=np.int32)

cos   = np.array([[c.rotate(p)['x'] for p in phase]])
cos_i = np.array([[c.rotate_int(p)['x'] for p in phase_i]])
cos_n = cos_i / (2 ** (n_bits - 1))
cos_err = cos_n - cos

sin   = np.array([[c.rotate(p)['y'] for p in phase]])
sin_i = np.array([[c.rotate_int(p)['y'] for p in phase_i]])
sin_n = sin_i / (2 ** (n_bits - 1))
sin_err = sin_n - sin

tbl = PrettyTable()
tbl.field_names = ["stat. val", "sin", "cos"]
tbl.add_rows([
    ["mean", np.mean(cos_err), np.mean(sin_err)],
    ["max", np.max(cos_err), np.max(sin_err)],
    ["min", np.min(cos_err), np.min(sin_err)],
    ["std", np.std(cos_err), np.std(sin_err)],
    ["max int", np.max(cos_i), np.max(sin_i)],
    ["min int", np.min(cos_i), np.min(sin_i)]
])
print(tbl)

fig, ax = plt.subplots()
ax.plot(phase, cos_err[0], phase, sin_err[0])
plt.show()