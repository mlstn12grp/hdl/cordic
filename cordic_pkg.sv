/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cordic_pkg;
	timeunit      1ns;
	timeprecision 1ps;

`include "cordic_atan_table.svh"


	typedef enum {
		ARCH_WORD_SERIAL,
		ARCH_FULLY_PIPELINED
	} architecture_et;

	typedef enum {
		MODE_ROTATE,
		MODE_VECTOR
	} mode_et;

	typedef enum {
		Q_1,
		Q_2,
		Q_3,
		Q_4
	} quadrant_et;

	typedef logic signed [$high(ATAN_TABLE[0]) + 1:$low(ATAN_TABLE[0])] data_int_at;

endpackage
