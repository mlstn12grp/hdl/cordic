/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import cordic_pkg::*;

module cordic_top (
	input logic i_rst,
	input logic i_clk,
	input mode_et i_mode,
	output logic o_ready,
	input logic i_valid,
	input logic [15:0] i_re_mag,
	input logic [15:0] i_im_pha,
	input logic i_ready,
	output mode_et o_mode,
	output logic o_valid,
	output logic [15:0] o_re_mag,
	output logic [15:0] o_im_pha
);
	timeunit      1ns;
	timeprecision 1ps;

	cordic_if #(16) i (i_rst, i_clk);
	cordic_if #(16) o (i_rst, i_clk);

	cordic #(
		.ARCH(ARCH_FULLY_PIPELINED)
	) c (
		.i(i),
		.o(o)
	);

	assign i.mode = i_mode;
	assign i.valid = i_valid;
	assign i.re_mag = i_re_mag;
	assign i.im_pha = i_im_pha;
	assign o.ready = i_ready;

	assign o_mode = o.mode;
	assign o_ready = i.ready;
	assign o_valid = o.valid;
	assign o_re_mag = o.re_mag;
	assign o_im_pha = o.im_pha;

endmodule
