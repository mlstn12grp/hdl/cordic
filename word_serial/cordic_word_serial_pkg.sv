/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cordic_word_serial_pkg;
	timeunit      1ns;
	timeprecision 1ps;


	import cordic_pkg::*;


	typedef logic [$clog2($size(ATAN_TABLE[0])) - 1:0] count_t;

	typedef enum {
		MS_IDLE,
		MS_ROTATE,
		MS_VECTOR
	} mode_state_et;

	typedef enum {
		S_IDLE,
		S_NORMALIZE,
		S_ROTATE,
		S_SCALE,
		S_SCALE_2,
		S_FINALIZE
	} cordic_state_et;

	typedef struct {
		mode_state_et   m_state;
		cordic_state_et state;
		mode_et         mode;
		data_int_at     re;
		data_int_at     re2;
		data_int_at     im;
		data_int_at     im2;
		data_int_at     pha;
		count_t         count;
		quadrant_et     quadrant;
		logic           ready;
		logic           valid;
	} state_t;

	const state_t STATE_RST = '{
		/* m_state  = */ MS_IDLE,
		/* state    = */ S_IDLE,
		/* mode     = */ MODE_ROTATE,
		/* re       = */ 0,
		/* re2      = */ 0,
		/* im       = */ 0,
		/* im2      = */ 0,
		/* pha      = */ 0,
		/* count    = */ 0,
		/* quadrant = */ Q_1,
		/* ready    = */ 0,
		/* valid    = */ 0
	};

endpackage
