/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import cordic_pkg::*;

module cordic_arch_word_serial(
	cordic_if.sink   i,
	cordic_if.source o
);
	timeunit      1ns;
	timeprecision 1ps;

	import cordic_word_serial_pkg::*;

	state_t r, w;


	assign o.mode   = r.mode;
	assign i.ready  = r.ready;
	assign o.valid  = r.valid;
	assign o.re_mag = r.re;
	assign o.im_pha = r.im;


	property no_re_overflow;
		@(posedge i.clk)
		disable iff (r.state != S_FINALIZE)
		w.re[$high(w.re):$high(w.re)-1] != 2'b01;
	endproperty
	property no_re_underflow;
		@(posedge i.clk)
		disable iff (r.state != S_FINALIZE)
		w.re[$high(w.re):$high(w.re)-1] != 2'b10;
	endproperty
	property no_im_overflow;
		@(posedge i.clk)
		disable iff (r.state != S_FINALIZE)
		w.im[$high(w.im):$high(w.im)-1] != 2'b01;
	endproperty
	property no_im_underflow;
		@(posedge i.clk)
		disable iff (r.state != S_FINALIZE)
		w.im[$high(w.im):$high(w.im)-1] != 2'b10;
	endproperty

	a_no_re_overflow:
	assert property (no_re_overflow)
	else $error("Real / magnitude overflow");
	a_no_re_underflow:
	assert property (no_re_underflow)
	else $error("Real / magnitude underflow");
	a_no_im_overflow:
	assert property (no_re_overflow)
	else $error("Imag. / phase overflow");
	a_no_im_underflow:
	assert property (no_re_underflow)
	else $error("Imag. / phase underflow");


	always_comb
	begin
		w <= r;

		unique case (r.m_state)
		MS_IDLE: begin
			w.ready <= 1;
			if (r.ready && i.valid) begin
				unique case (i.mode)
				MODE_ROTATE: begin
					w.m_state <= MS_ROTATE;
					w.re <= {i.re_mag[$high(i.re_mag)], i.re_mag <<< i.N_EXPAND};
					w.im <= 0;
					w.pha <= {i.im_pha[$high(i.im_pha)], i.im_pha <<< i.N_EXPAND};
				end
				MODE_VECTOR: begin
					w.m_state <= MS_VECTOR;
					w.re <= {i.re_mag[$high(i.re_mag)], i.re_mag <<< i.N_EXPAND};
					w.im <= {i.im_pha[$high(i.im_pha)], i.im_pha <<< i.N_EXPAND};
					w.pha <= 0;
				end
				endcase
				w.state <= S_NORMALIZE;
				w.mode <= i.mode;
				w.ready <= 0;
				w.valid <= 0;
			end else if (r.valid && i.ready) begin
				w.valid <= 0;
			end
		end
		MS_ROTATE: begin
			unique case (r.state)
			S_IDLE: begin
			end
			S_NORMALIZE: begin
				unique case (r.pha[$high(r.pha)-1:$high(r.pha)-2])
				2'b00: begin
					w.quadrant <= Q_1;
				end
				2'b01: begin
					w.quadrant <= Q_2;
				end
				2'b10: begin
					w.quadrant <= Q_3;
				end
				2'b11: begin
					w.quadrant <= Q_4;
				end
				default: begin
				end
				endcase
				w.pha <= r.pha <<< 2;
				w.pha[$high(w.pha)] <= 0;
				w.count <= 0;
				w.state <= S_ROTATE;
			end
			S_ROTATE: begin
				unique case (r.pha[$high(r.pha)])
				1'b0: begin
					w.re  <= r.re  - (r.im >>> r.count);
					w.im  <= r.im  + (r.re >>> r.count);
					w.pha <= r.pha - ATAN_TABLE[r.count];
				end
				1'b1: begin
					w.re  <= r.re  + (r.im >>> r.count);
					w.im  <= r.im  - (r.re >>> r.count);
					w.pha <= r.pha + ATAN_TABLE[r.count];
				end
				default: begin
				end
				endcase
				if (r.count < $high(ATAN_TABLE[0])) begin
					w.count <= r.count + 1;
				end else begin
					w.state <= S_SCALE;
				end
			end
			S_SCALE: begin
				w.re2 <= r.re;
				w.im2 <= r.im;
				if ($size(ATAN_TABLE[0]) < 10) begin
					w.re <=  ((r.re >>> 1)
						+ (r.re >>> 3)
						- (r.re >>> 6)
						- (r.re >>> 8)
						) >>> i.N_EXPAND;
					w.im <=  ((r.im >>> 1)
						+ (r.im >>> 3)
						- (r.im >>> 6)
						- (r.im >>> 8)
						) >>> i.N_EXPAND;
					w.state <= S_FINALIZE;
				end else begin
					w.re <=  ((r.re >>> 1)
						+ (r.re >>> 3)
						- (r.re >>> 6)
						- (r.re >>> 8)
						);
					w.im <=  ((r.im >>> 1)
						+ (r.im >>> 3)
						- (r.im >>> 6)
						- (r.im >>> 8)
						);
					w.state <= S_SCALE_2;
				end
			end
			S_SCALE_2: begin
				w.re <=  ((r.re)
					+ (r.re2 >>> 10)
					+ (r.re2 >>> 11)
					+ (r.re2 >>> 12)
					+ (r.re2 >>> 14)
					) >>> i.N_EXPAND;
				w.im <=  ((r.im)
					+ (r.im2 >>> 10)
					+ (r.im2 >>> 11)
					+ (r.im2 >>> 12)
					+ (r.im2 >>> 14)
					) >>> i.N_EXPAND;
				w.state <= S_FINALIZE;
			end
			S_FINALIZE: begin
				unique case (r.quadrant)
				Q_1: begin
				end
				Q_2: begin
					w.re <= -r.im;
					w.im <=  r.re;
				end
				Q_3: begin
					w.re <= -r.re;
					w.im <= -r.im;
				end
				Q_4: begin
					w.re <=  r.im;
					w.im <= -r.re;
				end
				endcase
				w.state <= S_IDLE;
				w.m_state <= MS_IDLE;
				w.valid <= 1;
			end
			endcase
		end
		MS_VECTOR: begin
			unique case (r.state)
			S_IDLE: begin
			end
			S_NORMALIZE: begin
				unique case ({r.re[$high(r.re)], r.im[$high(r.im)]})
				2'b00: begin
					w.quadrant <= Q_1;
				end
				2'b10: begin
					w.quadrant <= Q_2;
					w.re <=  r.im;
					w.im <= -r.re;
				end
				2'b11: begin
					w.quadrant <= Q_3;
					w.re <= -r.re;
					w.im <= -r.im;
				end
				2'b01: begin
					w.quadrant <= Q_4;
					w.re <= -r.im;
					w.im <=  r.re;
				end
				default: begin
				end
				endcase
				w.state <= S_ROTATE;
				w.count <= 0;
			end
			S_ROTATE: begin
				unique case (r.im[$high(r.im)])
				1'b0: begin
					w.re  <= r.re  + (r.im >>> r.count);
					w.im  <= r.im  - (r.re >>> r.count);
					w.pha <= r.pha + ATAN_TABLE[r.count];
				end
				1'b1: begin
					w.re  <= r.re  - (r.im >>> r.count);
					w.im  <= r.im  + (r.re >>> r.count);
					w.pha <= r.pha - ATAN_TABLE[r.count];
				end
				default: begin
				end
				endcase
				if (r.count < $high(ATAN_TABLE[0])) begin
					w.count <= r.count + 1;
				end else begin
					w.state <= S_SCALE;
				end
			end
			S_SCALE: begin
				/* Phase truncation */
				case (r.pha[$high(r.pha):$high(r.pha)-1])
				2'b11: begin
					w.pha <= 0;
				end
				2'b10: begin
					w.pha <= -1;
					w.pha[$high(w.pha)] <= 1'b0;
				end
				default: begin
				end
				endcase

				w.re2 <= r.re;

				if ($size(ATAN_TABLE[0]) < 10) begin
					w.re <=  ((r.re >>> 1)
						+ (r.re >>> 3)
						- (r.re >>> 6)
						- (r.re >>> 8)
						) >>> i.N_EXPAND;
					w.state <= S_FINALIZE;
				end else begin
					w.re <=  ((r.re >>> 1)
						+ (r.re >>> 3)
						- (r.re >>> 6)
						- (r.re >>> 8)
						);
					w.state <= S_SCALE_2;
				end
			end
			S_SCALE_2: begin
				w.re <=  ((r.re)
					+ (r.re >>> 10)
					+ (r.re >>> 11)
					+ (r.re >>> 12)
					+ (r.re >>> 14)
					) >>> i.N_EXPAND;
				w.state <= S_FINALIZE;
			end
			S_FINALIZE: begin
				unique case (r.quadrant)
				Q_1: begin
					w.pha <= (r.pha >>> 2);
					w.im  <= (r.pha >>> 2);
				end
				Q_2: begin
					w.pha <= (r.pha >>> 2) + (2 ** ($size(i.re_mag) - 2));
					w.im  <= (r.pha >>> 2) + (2 ** ($size(i.re_mag) - 2));
				end
				Q_3: begin
					w.pha <= (r.pha >>> 2) - (2 ** ($size(i.re_mag) - 1));
					w.im  <= (r.pha >>> 2) - (2 ** ($size(i.re_mag) - 1));
				end
				Q_4: begin
					w.pha <= (r.pha >>> 2) - (2 ** ($size(i.re_mag) - 2));
					w.im  <= (r.pha >>> 2) - (2 ** ($size(i.re_mag) - 2));
				end
				endcase
				/* Magnitude truncation */
				unique case (r.re[$high(r.re):$high(r.re)-1])
				2'b01: begin
					w.re <= -1;
					w.re[$high(w.re):$high(w.re)-1] <= 2'b00;
				end
				2'b10: begin
					w.re <= 0;
					w.re[$high(w.re):$high(w.re)-1] <= 2'b11;
				end
				default: begin
				end
				endcase
				w.state <= S_IDLE;
				w.m_state <= MS_IDLE;
				w.valid <= 1;
			end
			endcase
		end
		endcase
	end

	always_ff @(posedge i.clk, posedge i.rst)
	begin
		if (i.rst) begin
			r <= STATE_RST;
		end else begin
			r <= w;
		end
	end

endmodule
