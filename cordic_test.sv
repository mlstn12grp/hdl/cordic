/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
program cordic_test (
	cordic_if.sink   i,
	cordic_if.source o
);
	timeunit      1ns;
	timeprecision 1ps;

	import cordic_pkg::*;

	const int unsigned N_WIDTH = i.WIDTH;

	initial
	begin
		for (int a = -(2**(N_WIDTH-1)); a < (2**(N_WIDTH-1)); ++a) begin
			wait (o.ready);

			o.mode = MODE_ROTATE;
			o.valid = 1;
			o.re_mag = ((2**(N_WIDTH-1)) - 1);
			o.im_pha = a;

			@(posedge i.clk);
			o.valid = 0;
		end

		wait (i.valid);

		for (int a = -(2**(N_WIDTH-1)); a < (2**(N_WIDTH-1)); ++a) begin
			real phase = (2 * 3.141592653589793) * (real'(a) / (2**N_WIDTH));
			wait (o.ready);

			o.mode = MODE_VECTOR;
			o.valid = 1;
			o.re_mag = int'($floor((2**(N_WIDTH-1)-1) * $cos(phase)));
			o.im_pha = int'($floor((2**(N_WIDTH-1)-1) * $sin(phase)));

			@(posedge i.clk);
			o.valid = 0;
		end

		wait (i.valid);

		$finish;
	end

endprogram
