/*
 * Copyright 2021 Alexander Preissner
 * SPDX-License-Identifier: Apache-2.0 WITH SHL-2.1
 * 
 * Licensed under the Solderpad Hardware License v 2.1 (the “License”);
 * you may not use this file except in compliance with the License, or, at your
 * option, the Apache License version 2.0.
 * You may obtain a copy of the License at
 * 
 * https://solderpad.org/licenses/SHL-2.1/
 * 
 * Unless required by applicable law or agreed to in writing, any work
 * distributed under the License is distributed on an “AS IS” BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
module cordic_tb #(
	parameter CLK_FREQ = 1.0e6,
	parameter WIDTH    = 16
);
	timeunit      1ns;
	timeprecision 1ps;

	import cordic_pkg::*;

	logic w_rst;
	logic w_clk;

	cordic_if #(WIDTH) i (.rst(w_rst), .clk(w_clk));
	cordic_if #(WIDTH) o (.rst(w_rst), .clk(w_clk));

	data_int_at w_re_mag, w_im_pha;

	cordic # (
		.ARCH(ARCH_WORD_SERIAL)
		// .ARCH(ARCH_FULLY_PIPELINED)
	) uut (
		.i(i),
		.o(o)
	);
	cordic_test the_test (
		.i(o),
		.o(i)
	);

	initial
	begin
		$dumpfile("test.vcd");
		$dumpvars(0,cordic_tb);
	end

	initial
	begin
		realtime clk_period;
		clk_period = 1.0 / CLK_FREQ * 1.0e9;

		w_clk = 0;

		forever
		begin
			# (clk_period / 2.0);
			w_clk = ~w_clk;
		end
	end

	initial
	begin
		w_rst <= 1;
		@(posedge w_clk);
		w_rst <= 0;
	end

	always_comb
	begin
		if (o.valid) begin
			w_re_mag <= o.re_mag;
			w_im_pha <= o.im_pha;
		end
	end

endmodule
